module.exports = function(grunt) {

	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		connect: {
			server: {
				options: {
					hostname: '*',
					port: 8000,
					base: './app',
					livereload: true
				}
			}
		},

		watch: {
			all: {
				// All files in current directory, all files 1 directory down, and all files in the app/js directory
				files: [ '*.*', 'app/*.*', 'app/js/*.js'],
				options: {
					livereload: true
				}
			}
		}
	});

	grunt.registerTask('serve', ['connect', 'watch']);

	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-watch');

}