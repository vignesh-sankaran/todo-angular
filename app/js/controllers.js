'use strict';

var todoAngularApp = angular.module('todoangularApp', ['TaskModule', 'TaskListStorage', 'EditTaskData', 'ui.router']);

todoAngularApp.run(function ($rootScope, $state, $stateParams) {
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;
});

todoAngularApp.controller('todoCtrl', ['$rootScope', '$scope', 'Task', 'taskListStorageService', 'editTaskDataService', '$state',
	function ($rootScope, $scope, Task, taskListStorageService, editTaskDataService, $state) {

	var DECIMAL_SYSTEM = 10

	$scope.taskList = taskListStorageService.Restore() || [];

	$rootScope.$on('taskEdited')
	{
		var editedTask = editTaskDataService.RetrieveTaskToEdit();

		$scope.taskList[editedTask.id] = editedTask;

		taskListStorageService.Save($scope.taskList);
	}

	$scope.getTaskEditState = function(taskId)
	{
		var paramsTaskId = parseInt($state.params.taskId, DECIMAL_SYSTEM);

		if (paramsTaskId === taskId)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	$scope.passEditTask = function(taskId)
	{
		editTaskDataService.SetTaskValue($scope.taskList[taskId]);
	}

	$scope.addTask = function() {

		Task.AddTask($scope.taskName, $scope.taskDueDate, $scope.taskDueTime, $scope.taskList);

		// Reset the text fields to being empty
		$scope.taskName = "";
		$scope.taskDueDate = "";
		$scope.taskDueTime = "";
	};

	$scope.clearFields = function() {
		$scope.taskName = "";
		$scope.taskDueDate = "";
		$scope.taskDueTime = "";
	};

}]);

todoAngularApp.controller('editTask', ['$rootScope', '$scope', 'Task', 'editTaskDataService', 'taskListStorageService',
	function($rootScope, $scope, Task, editTaskDataService, taskListStorageService) {

	$scope.taskToEdit = editTaskDataService.RetrieveTaskToEdit();

	$scope.saveTaskEdit = function()
	{
		$scope.taskList[$scope.taskToEdit.id] = $scope.taskToEdit;
		taskListStorageService.Save($scope.taskList);
	};
}]);
