var todoAngularApp = angular.module('todoangularApp');

todoAngularApp.config(function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/list'),

	$stateProvider
	.state('todo', {
		url: '/list',
		views:
		{
			'todo': { templateUrl: 'list.html', controller: 'todoCtrl' },
			'viewTask@todo': { templateUrl: 'viewTask.html'}
		}
	})

	.state('todo.editTask', {
		url: '/:taskId',
		views:
		{
			'editTask@todo': { templateUrl: 'editTask.html', controller: 'editTask'}
		}

	});
});