// Local storage service
var taskListStorage = angular.module('TaskListStorage', []);
taskListStorage.factory('taskListStorageService', [function () {
	var factory =
	{
		Save: function(taskList)
		{
			// First parameter is the name of the local storage item, second parameter is turning the actual array into JSON
			localStorage.setItem('taskList', angular.toJson(taskList));
		},

		Restore: function()
		{
			if (localStorage.getItem('taskList') === null || localStorage.getItem('taskList') === "")
				return null;
			else
				return angular.fromJson(localStorage.getItem('taskList'));
		},
	};

	return factory;
}]);

var task = angular.module('TaskModule', ['TaskListStorage']);
task.factory('Task', ['taskListStorageService', '$rootScope', function(taskListStorageService, $rootScope) {
	var factory =
	{
		AddTask: function(taskName, taskDueDate, taskDueTime, taskList)
		{
			var newItemIndex = taskList.length; // index counting is done on a 0 index counting, so new task's id will be the length of the array

			if (taskName != "" && taskDueDate != "" && taskDueTime != "")
			{
				taskList.push( {id: newItemIndex, completed: false, name: taskName, dueDate: taskDueDate, dueTime: taskDueTime} );
				taskListStorageService.Save(taskList);
			}
		},

		RetrieveTask: function(taskId)
		{
			var taskList = taskListStorageService.Restore();

			for (var i = taskList.length - 1; i >= 0; i--) {
				if (taskList[i].id === taskId)
					return taskList[i];
			}
		}
	};

	return factory;
}]);

var editTaskData = angular.module('EditTaskData', []);
editTaskData.factory('editTaskDataService', ['$rootScope', function($rootScope) {
	var taskToEdit = {};

	var factory =
	{
		RetrieveTaskToEdit: function()
		{
			return taskToEdit;
		},

		SetTaskValue: function(task)
		{
			taskToEdit = task;
		},

		EditTaskValue: function(task)
		{
			taskToEdit = task;
			$rootScope.$broadcast('taskEdited');
		}
	};

	return factory;
}]);