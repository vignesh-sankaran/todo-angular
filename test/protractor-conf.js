exports.config = {
	allScriptsTimeout: 10000,

	specs: [
		"e2e/*.js"
	],

	capabilities: {
		"browserName": "firefox"
	},

	chromeOnly: false;

	baseUrl: "http://localhost/8000",

	framework: "jasmine",

	jasmineNodeOpts: {
    	defaultTimeoutInterval: 30000
  	}
}