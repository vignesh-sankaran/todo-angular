'use strict';

describe('todoCtrl', function() {
	var rootScope, scope, Task, taskListStorageService, state;

	beforeEach(module('todoangularApp'));
	beforeEach(module('TaskModule'));
	beforeEach(module('TaskListStorage'));
	beforeEach(module('todoCtrl'));

	beforeEach(inject(function($scope, _Task_, _taskListStorageService_, $state) {
		scope = $rootScope.$new();
		Task = _Task_;
		taskListStorageService = _taskListStorageService_;
	}));
}